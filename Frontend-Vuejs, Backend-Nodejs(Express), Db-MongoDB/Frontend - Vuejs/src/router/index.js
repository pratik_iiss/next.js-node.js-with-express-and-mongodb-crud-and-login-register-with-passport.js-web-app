import { createRouter, createWebHistory } from 'vue-router';

// Define routes for authenticated users
const authenticatedRoutes = [
  {
    path: '/dashboard',
    component: () => import('../pages/dashboard.vue'),
  },
  {
    path: '/account-settings',
    component: () => import('../pages/account-settings.vue'),
  },
  {
    path: '/typography',
    component: () => import('../pages/typography.vue'),
  },
  {
    path: '/icons',
    component: () => import('../pages/icons.vue'),
  },
  {
    path: '/cards',
    component: () => import('../pages/cards.vue'),
  },
  {
    path: '/tables',
    component: () => import('../pages/tables.vue'),
  },
  {
    path: '/veucrud',
    component: () => import('../pages/veucrud.vue'),
  },
  {
    path: '/form-layouts',
    component: () => import('../pages/form-layouts.vue'),
  },
];

// Define routes for non-authenticated users
const nonAuthenticatedRoutes = [
  {
    path: '/login',
    component: () => import('../pages/login.vue'),
  },
  {
    path: '/register',
    component: () => import('../pages/register.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import('../pages/[...all].vue'),
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // Redirect root path to login page if user is not authenticated
    { path: '/', redirect: '/login' },

    // Routes for authenticated users
    {
      path: '/',
      component: () => import('../layouts/default.vue'),
      children: authenticatedRoutes,
    },
    
    // Routes for non-authenticated users
    {
      path: '/',
      component: () => import('../layouts/blank.vue'),
      children: nonAuthenticatedRoutes,
    },
  ],
});

export default router;
