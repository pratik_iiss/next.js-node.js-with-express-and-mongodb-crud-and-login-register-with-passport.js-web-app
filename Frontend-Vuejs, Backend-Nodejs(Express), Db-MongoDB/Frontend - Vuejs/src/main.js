import App from '@/App.vue';
import vuetify from '@/plugins/vuetify';
import { loadFonts } from '@/plugins/webfontloader';
import router from '@/router';
import '@core/scss/template/index.scss';
import '@layouts/styles/index.scss';
import '@styles/styles.scss';
import { createPinia } from 'pinia';
import { createApp } from 'vue';
import Toast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css'; // Or choose another theme

loadFonts();

const app = createApp(App);

app.use(vuetify);
app.use(createPinia());
app.use(router);
app.use(Toast);

app.mount('#app');
