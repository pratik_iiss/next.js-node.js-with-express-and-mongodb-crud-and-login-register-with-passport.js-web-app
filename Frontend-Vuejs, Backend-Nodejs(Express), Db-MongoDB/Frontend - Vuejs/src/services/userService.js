import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'http://localhost:3001/api',
  withCredentials: false, // This is the default
});

export default {
  getUsers() {
    return apiClient.get('/users');
  },
  getUser(id) {
    if (!id) {
      throw new Error("User ID is required");
    }
    return apiClient.get(`/users/${id}`);
  },
  createUser(user) {
    return apiClient.post('/users', user);
  },
  updateUser(id, user) {
    if (!id) {
      throw new Error("User ID is required");
    }
    return apiClient.put(`/users/${id}`, user);
  },
  deleteUser(id) {
    if (!id) {
      throw new Error("User ID is required");
    }
    return apiClient.delete(`/users/${id}`);
  },
  registerUser(user) {
    return apiClient.post('/register', user);
  },
  
  // loginUser(credentials) {
  //   return apiClient.post('/login', credentials);
  // },
};
