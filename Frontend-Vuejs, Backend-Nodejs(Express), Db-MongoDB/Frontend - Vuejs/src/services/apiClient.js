// src/services/apiClient.js
import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'http://localhost:3001/api', // Adjust base URL as needed
  headers: {
    'Content-Type': 'application/json',
  },
});

// Define API methods
const apiMethods = {
  loginUser: (credentials) => apiClient.post('/login', credentials),
};

export default apiMethods;
