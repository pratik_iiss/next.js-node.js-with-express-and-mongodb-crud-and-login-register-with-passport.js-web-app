//app.js
const express = require('express');
const path = require('path');
const app = express();
const port = 3001;

// Set EJS as the view engine
app.set('view engine', 'ejs');

// Set the views directory
app.set('views', path.join(__dirname, 'views'));

// Middleware to serve static files
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', require('./routes/index'));

// Start the server
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});
