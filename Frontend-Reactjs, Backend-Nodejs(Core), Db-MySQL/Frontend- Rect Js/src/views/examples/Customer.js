import React, { useState } from "react";
import axios from 'axios'; // Import Axios for making HTTP requests
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// reactstrap components
import { Card, Container, Row, Form, FormGroup, Label, Input, Button } from "reactstrap";

// core components
import Header from "components/Headers/Header.js";
import { useNavigate } from 'react-router-dom'; // Add this import

const Customer = () => {
    // Define state variables to store form data and success message
    const navigate = useNavigate(); // Initialize the navigate function

    const [formData, setFormData] = useState({
      email: "",
      first_name: "",
      last_name: "",
      profile: ""

    });
    const [successMessage, setSuccessMessage] = useState("");

    // Handle form input change
    const handleInputChange = (e) => {
      const { name, value } = e.target;
      setFormData({
        ...formData,
        [name]: value
      });
    };
  
    // Handle form submission
    const handleSubmit = async (e) => {
      e.preventDefault();
      
      try {
        // Make an HTTP POST request to your backend API endpoint
        const response = await axios.post('http://localhost:3001/api/store/users', formData);
        
        // If the request is successful, display a toast message and redirect to the new page
        toast.success(response.data.message);
        navigate('/admin/tables', { replace: true }); // Redirect to the new page
      } catch (error) {
        // If there's an error, log the error and display an error message to the user
        if (error.response) {
          toast.error(error.response.data.error);
        } else {
          toast.error('An error occurred while submitting the form. Please try again.');
        }
      }
    };
  
    return (
      <>
        <Header />
        <Container className="mt--7" fluid>
          <Row>
            <div className="col">
              <Card className="shadow border-0">
                <Form onSubmit={handleSubmit} className="p-4" >
                  <FormGroup>
                    <Label for="name">Email</Label>
                    <Input
                      type="text"
                      name="email"
                      id="email"
                      placeholder="Enter your email"
                      value={formData.email}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="email">First Name</Label>
                    <Input
                      type="text"
                      name="first_name"
                      id="first_name"
                      placeholder="Enter your first name"
                      value={formData.first_name}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="email">last name</Label>
                    <Input
                      type="text"
                      name="last_name"
                      id="last_name"
                      placeholder="Enter your last name"
                      value={formData.last_name}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label for="profile">Profile</Label>
                    <Input
                      type="file"
                      name="profile"
                      id="profile"
                      placeholder="Enter your profile"
                      value={formData.profile}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                  <Button color="primary" type="submit">Submit</Button>
                  {/* {successMessage && <p className="text-success mt-2">{successMessage}</p>} */}
                </Form>
              </Card>
            </div>
          </Row>
        </Container>
        <ToastContainer />
      </>
    );
  };

export default Customer;
