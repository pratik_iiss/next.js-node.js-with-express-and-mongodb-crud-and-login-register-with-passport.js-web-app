import React, { useEffect, useMemo, useState } from "react";
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { useTable, usePagination, useSortBy, useGlobalFilter } from 'react-table';

// core components
import Header from "components/Headers/Header.js";

const Tables = () => {
  const [users, setUsers] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false); // State for delete confirmation modal
  const [userToDelete, setUserToDelete] = useState(null);

  useEffect(() => {
    fetch('http://localhost:3001/api/users')
      .then(response => response.json())
      .then(data => setUsers(data))
      .catch(error => console.error('Error fetching data:', error));
  }, []);

  const data = useMemo(() => users, [users]);

  const handleEdit = (userId) => {
    fetch(`http://localhost:3001/api/users/${userId}`)
      .then(response => response.json())
      .then(data => {
        setSelectedUser(data);
        setIsModalOpen(true);
      })
      .catch(error => console.error('Error fetching user:', error));
  };

  const saveUser = (updatedUser) => {
    fetch(`http://localhost:3001/api/users/${updatedUser.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedUser),
    })
      .then(response => response.json())
      .then(data => {
        if (data.error) {
          console.error('Error updating user:', data.error);
        } else {
          console.log('User updated successfully:', data.message);
          // Update the users state with the updated user details
          setUsers(users.map(user => (user.id === updatedUser.id ? updatedUser : user)));
        }
      })
      .catch(error => console.error('Error updating user:', error));
  };

  const handleDelete = (userId) => {
    const userToDelete = users.find(user => user.id === userId);
    setUserToDelete(userToDelete);  // Update userToDelete state
    setDeleteModalOpen(true);  
  };

  const confirmDelete = () => {
    if (!userToDelete) return;
  
    fetch(`http://localhost:3001/api/users/${userToDelete.id}`, {
      method: 'DELETE',
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to delete user');
        }
        // Update users state after successful deletion
        setUsers(users.filter(user => user.id !== userToDelete.id));
        setDeleteModalOpen(false);  // Close delete confirmation modal
      })
      .catch(error => console.error('Error deleting user:', error));
  };


  const columns = useMemo(
    () => [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'First Name',
        accessor: 'first_name',
      },
      {
        Header: 'Last Name',
        accessor: 'last_name',
      },
      // {
      //   Header: 'User Profile',
      //   accessor: 'profile',
      // },
      {
        Header: 'Actions',
        accessor: 'actions',
        Cell: ({ row }) => (
          <div>
            <Button color="warning" size="sm" onClick={() => handleEdit(row.original.id)}>
              Edit
            </Button>{' '}
            <Button color="danger" size="sm" onClick={() => handleDelete(row.original.id)}>
              Delete
            </Button>
          </div>
        ),
      },
    ],
    []
  );


  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    setGlobalFilter,
    state: { pageIndex, pageSize, globalFilter },
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 10 }, // Adjust page size if needed
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Card tables</h3>
                <input
                  value={globalFilter || ''}
                  onChange={e => setGlobalFilter(e.target.value)}
                  placeholder="Search"
                />
              </CardHeader>
              <Table className="align-items-center table-flush" responsive {...getTableProps()}>
                <thead className="thead-light">
                  {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                      {headerGroup.headers.map(column => (
                        <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                          {column.render('Header')}
                          <span>
                            {column.isSorted
                              ? column.isSortedDesc
                                ? ' 🔽'
                                : ' 🔼'
                              : ''}
                          </span>
                        </th>
                      ))}
                    </tr>
                  ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                  {page.map(row => {
                    prepareRow(row);
                    return (
                      <tr {...row.getRowProps()}>
                        {row.cells.map(cell => (
                          <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                        ))}
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={!canPreviousPage}>
                      <PaginationLink onClick={() => gotoPage(0)} first />
                    </PaginationItem>
                    <PaginationItem disabled={!canPreviousPage}>
                      <PaginationLink onClick={() => previousPage()} previous />
                    </PaginationItem>
                    <PaginationItem disabled={!canNextPage}>
                      <PaginationLink onClick={() => nextPage()} next />
                    </PaginationItem>
                    <PaginationItem disabled={!canNextPage}>
                      <PaginationLink onClick={() => gotoPage(pageCount - 1)} last />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
      <Modal isOpen={deleteModalOpen} toggle={() => setDeleteModalOpen(!deleteModalOpen)}>
        <ModalHeader toggle={() => setDeleteModalOpen(!deleteModalOpen)}>Confirm Delete</ModalHeader>
        <ModalBody>
          Are you sure you want to delete user with ID {userToDelete && userToDelete.id}?
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={confirmDelete}>Delete</Button>{' '}
          <Button color="secondary" onClick={() => setDeleteModalOpen(!deleteModalOpen)}>Cancel</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={isModalOpen} toggle={() => setIsModalOpen(!isModalOpen)}>
        <ModalHeader toggle={() => setIsModalOpen(!isModalOpen)}>Edit User</ModalHeader>
        <ModalBody>
          {selectedUser && (
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                saveUser(selectedUser);
                setIsModalOpen(false);
              }}
            >
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  id="email"
                  value={selectedUser.email}
                  onChange={(e) =>
                    setSelectedUser({ ...selectedUser, email: e.target.value })
                  }
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="first_name">First Name</Label>
                <Input
                  type="text"
                  id="first_name"
                  value={selectedUser.first_name}
                  onChange={(e) =>
                    setSelectedUser({ ...selectedUser, first_name: e.target.value })
                  }
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label for="last_name">Last Name</Label>
                <Input
                  type="text"
                  id="last_name"
                  value={selectedUser.last_name}
                  onChange={(e) =>
                    setSelectedUser({ ...selectedUser, last_name: e.target.value })
                  }
                  required
                />
              </FormGroup>
              
              <Button type="submit" color="primary">
                Save
              </Button>
            </Form>
          )}
        </ModalBody>
      </Modal>
    </>
  );
};

export default Tables;