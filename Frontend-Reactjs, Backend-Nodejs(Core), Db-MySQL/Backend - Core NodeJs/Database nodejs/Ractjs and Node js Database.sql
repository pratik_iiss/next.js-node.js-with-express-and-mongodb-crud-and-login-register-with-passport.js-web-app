-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2024 at 02:24 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodejs`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(210) DEFAULT NULL,
  `first_name` varchar(210) DEFAULT NULL,
  `last_name` varchar(210) DEFAULT NULL,
  `profile` varchar(210) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `profile`) VALUES
(1, 'ranamihir123@gmail.com', 'Rana', 'Mihir', 'http://localhost/phpmyadmin/index.php?route=/table/change&db=nodejs&table=users'),
(2, 'new@gmail.com', 'Jariwala', 'mihir', 'http://localhost:3000/api/store/users'),
(3, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL),
(6, 'test12@gmail.com', NULL, NULL, NULL),
(7, 'test12@gmail.com', 'Jariwala', 'mihir', 'http://localhost:3000/api/store/users'),
(8, 'test12@gmail.com', 'Jariwala', 'mihir', 'http://localhost:3000/api/store/users'),
(9, 'test12@gmail.com', 'Jariwala', 'mihir', 'http://localhost:3000/api/store/users'),
(10, 'test1dcbhgchghgdhc@gmail.com', 'Jariwala', 'mihir', 'http://localhost:3000/api/store/users'),
(11, 'pratik473@yopmail.com', 'dw', 'edede', 'dd'),
(12, 'ojeevan@gmail.com', 'ed', 'edede', 'dd'),
(13, 'pratikrf473@yopmail.com', 'dw', 'edede', 'ranamihir1243@'),
(14, 'test12@yopmail.com', 'dw', 'edede', 'dd'),
(15, 'test1232@yopmail.com', 'sd', 'ds', 'sdsd'),
(16, 'pratik429@yopmail.com', 'Rana', 'mihir', 'The test'),
(17, 'pratik4921@yopmail.com', 'dw', 'edede', 'C:\\fakepath\\360_F_244436923_vkMe10KKKiw5bjhZeRDT05moxWcPpdmb.jpg'),
(18, 'ranamihir196@gmail.com', 'Rana', 'Mihir', 'C:\\fakepath\\anweshippin-kandethum.jpg'),
(19, 'pratik20062420@yopmail.com', 'mihir', 'rana', 'C:\\fakepath\\composer download.png'),
(20, 'Geoffrey', 'Odessa', 'Blake', 'C:\\fakepath\\composer download.png'),
(21, 'ranamihir1234@gmail.com', 'rana', 'mihir', 'C:\\fakepath\\Screenshot (1).png'),
(22, 'ranamihir12345@yopmail.com', 'mihir', 'rana', ''),
(23, 'Isaiah13@yopmail.com', 'Lucius', 'Mason', 'C:\\fakepath\\composer download.png'),
(24, 'test123@yopmail.com', 'Test', 'rana', 'C:\\fakepath\\Screenshot (2).png'),
(25, 'xssxsxss@gmail.com', 's', 's', ''),
(26, 'Colleen@gmail.com', 'test', 'Duffy', 'C:\\fakepath\\composer download.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
