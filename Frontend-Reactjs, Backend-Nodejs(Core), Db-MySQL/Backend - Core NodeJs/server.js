// server.js

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const pool = require('./db'); // Import the database pool from db.js

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors()); 


app.get('/', (req, res) => {
    res.send('Welcome to the Node.js API');
});


app.get('/api/users', (req, res) => {
  // Retrieve users from the database using the imported pool
  pool.query('SELECT * FROM users ORDER BY id DESC', (error, results) => {
    if (error) {
      console.error('Error executing query:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }
    res.json(results);
  });
});

app.post('/api/store/users', (req, res) => {
    const { email, first_name, last_name, profile } = req.body;
  
    // Check if required fields are provided
    if (!email) {
      return res.status(400).json({ error: 'Email is required' });
    }
  
    // Check if email is unique
    pool.query('SELECT * FROM users WHERE email = ?', email, (error, results) => {
        if (error) {
            console.error('Error executing query:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
        if (results.length > 0) {
            // Email already exists, return an error
            return res.status(400).json({ error: 'Email already exists' });
        }
        
        // Insert user into the database
        const query = 'INSERT INTO users (email, first_name, last_name, profile) VALUES (?, ?, ?, ?)';
        const values = [email, first_name, last_name, profile];
    
        pool.query(query, values, (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                return res.status(500).json({ error: 'Internal server error' });
            }
            res.status(201).json({ message: 'User created successfully', userId: results.insertId });
        });
    });
});


app.get('/api/users/:id', (req, res) => {
    const userId = req.params.id;
  
    pool.query('SELECT * FROM users WHERE id = ?', userId, (error, results) => {
      if (error) {
        console.error('Error executing query:', error);
        return res.status(500).json({ error: 'Internal server error' });
      }
      if (results.length === 0) {
        return res.status(404).json({ error: 'User not found' });
      }
      res.json(results[0]); // Assuming there's only one user with the specified ID
    });
  });
  
  // Route to update a user
  app.put('/api/users/:id', (req, res) => {
    const userId = req.params.id;
    const { email, first_name, last_name, profile } = req.body;
  
    // Check if the user exists
    pool.query('SELECT * FROM users WHERE id = ?', userId, (error, results) => {
      if (error) {
        console.error('Error executing query:', error);
        return res.status(500).json({ error: 'Internal server error' });
      }
      if (results.length === 0) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      // Update user information
      const query = 'UPDATE users SET email = ?, first_name = ?, last_name = ?, profile = ? WHERE id = ?';
      const values = [email, first_name, last_name, profile, userId];
  
      pool.query(query, values, (error, results) => {
        if (error) {
          console.error('Error executing query:', error);
          return res.status(500).json({ error: 'Internal server error' });
        }
        res.json({ message: 'User updated successfully' });
      });
    });
  });


  app.delete('/api/users/:id', (req, res) => {
    const userId = req.params.id;
  
    pool.query('DELETE FROM users WHERE id = ?', [userId], (error, results) => {
      if (error) {
        console.error('Error executing query:', error);
        return res.status(500).json({ error: 'Internal server error' });
      }
      if (results.affectedRows === 0) {
        return res.status(404).json({ error: 'User not found' });
      }
      res.json({ message: 'User deleted successfully' });
    });
  });
  
const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
