import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import UsersView from '../views/Users/View.vue'
import UsersCreate from '../Views/Users/Create.vue'
import EditUser from '../Views/Users/Edit.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/users',
      name: 'users',
      component: UsersView
    },
    {
      path: '/users/create',
      name: 'usercreate',
      component: UsersCreate
    },
    {
      path: '/users/:id/edit',
      name: 'useredit',
      component: EditUser
    },
  ]
})

export default router