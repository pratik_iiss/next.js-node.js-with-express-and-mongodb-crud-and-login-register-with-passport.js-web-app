// ** MUI Imports
import Grid from '@mui/material/Grid'
import Link from '@mui/material/Link'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import { Toaster } from 'react-hot-toast';

// ** Demo Components Imports

import TableCutomer from 'src/views/tables/TableCutomer'

const CRudTable = () => {
  return (
    <Grid container spacing={6}>
      <Toaster />
      <Grid item xs={12}>
        <Typography variant='h5'>
          <Link href='https://mui.com/components/tables/' target='_blank'>
            Next Js Crud
          </Link>
        </Typography>
        <Typography variant='body2'>Tables display sets of data. They can be fully customized</Typography>
      </Grid>
      <Grid item xs={12}>
        <Card>
          <CardHeader title='Customer Data' titleTypographyProps={{ variant: 'h6' }} />
          <TableCutomer />
        </Card>
      </Grid>

    </Grid>
  )
}

export default CRudTable;
