import { useState, ChangeEvent, useEffect } from 'react';
import { toast } from 'react-hot-toast';

// ** MUI Imports
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableRow from '@mui/material/TableRow';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import axios from 'axios';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import InputAdornment from '@mui/material/InputAdornment';

import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

const TableCustomer = () => {
  // ** States
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);
  const [rows, setRows] = useState<any[]>([]); // add a state to store the API data
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [selectedRow, setSelectedRow] = useState({});
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [selectedRowToDelete, setSelectedRowToDelete] = useState({});
  const [openCreateDialog, setOpenCreateDialog] = useState(false);
  const [newUser, setNewUser] = useState({});

  const handleDeleteClick = (row: any) => {
    setSelectedRowToDelete(row);
    setOpenDeleteDialog(true);
  };

  const handleDeleteDialogClose = () => {
    setOpenDeleteDialog(false);
  };

  const handleDeleteDialogConfirm = async () => {
    try {
      const userId = selectedRowToDelete.id;
      const response = await axios.delete(`http://localhost:3001/api/users/${userId}`);
      const { message } = response.data;
      console.log(`User deleted successfully: ${message}`);
      // Update the state to remove the deleted user
      setRows(rows.filter((row) => row.id !== userId));
      setOpenDeleteDialog(false);
      toast.success(`${message}`);
    } catch (error) {
      console.error(error);
      toast.error(`Error deleting user: ${error.message}`);
    }
  };

  const handleEditClick = (row: any) => {
    setSelectedRow(row);
    setOpenEditDialog(true);
  };

  const handleEditDialogClose = () => {
    setOpenEditDialog(false);
  };

  const handleCreateUser = async () => {
    try {
      const response = await axios.post('http://localhost:3001/api/store/users', newUser);
      const { message, userId } = response.data;
      console.log(`User created successfully: ${message}, userId: ${userId}`);
      // Update the state with the new user data
      setRows([...rows, newUser]);
      // Set the popup message
      toast.success(`${message}`);
      // **Fetch the updated list of users**
      axios.get('http://localhost:3001/api/users')
        .then((response) => {
          setRows(response.data);
        })
        .catch((error) => {
          console.error(error);
          toast.error(error.error);
        });
    } catch (error) {
      if (error.error) {
        toast.error(error.error);
      } else {
        toast.error(`Error creating user`);
      }
    }
  };

  const handleCreateClick = () => {
    setOpenCreateDialog(true);
  };

  const handleCreateDialogClose = () => {
    setOpenCreateDialog(false);
  };

  const handleCreateDialogSave = () => {
    handleCreateUser();
    setOpenCreateDialog(false);
  };

  const handleEditDialogSave = async () => {
    try {
      const userId = selectedRow.id;
      const { email, first_name, last_name, profile } = selectedRow;

      const response = await axios.put(`http://localhost:3001/api/users/${userId}`, {
        email,
        first_name,
        last_name,
        profile,
      });

      const { message } = response.data;
      console.log(`User updated successfully: ${message}`);
      // Update the state with the updated user data
      setRows(rows.map((row) => (row.id === userId ? selectedRow : row)));
      // Close the edit dialog
      setOpenEditDialog(false);
      toast.success(`${message}`);
    } catch (error) {
      console.error(error);
      toast.error(`Error updating user`);
    }
  };

  useEffect(() => {
    axios.get('http://localhost:3001/api/users')
      .then((response) => {
        setRows(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [newUser]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <Button
        variant='contained'
        color='primary'
        size='small'
        onClick={handleCreateClick}
        sx={{
          position: 'absolute',
          top: '20%',
          left: '90%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        Add User
      </Button>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell key='id' align='right'>
                ID
              </TableCell>
              <TableCell key='email' align='right'>
                Email
              </TableCell>
              <TableCell key='first_name' align='right'>
                First Name
              </TableCell>
              <TableCell key='last_name' align='right'>
                Last Name
              </TableCell>
              <TableCell key='edit' align='right'>
                Edit
              </TableCell>
              <TableCell key='delete' align='right'>
                Delete
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role='checkbox' tabIndex={-1} key={row.id}>
                  <TableCell align='right'>{row.id}</TableCell>
                  <TableCell align='right'>{row.email}</TableCell>
                  <TableCell align='right'>{row.first_name}</TableCell>
                  <TableCell align='right'>{row.last_name}</TableCell>
                  <TableCell align='right'>
                    <Button variant='contained' color='primary' size='small' onClick={() => handleEditClick(row)} >
                      Edit User
                    </Button>
                  </TableCell>
                  <TableCell align='right'>
                    <Button variant='contained' color='error' size='small' onClick={() => handleDeleteClick(row)}>
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component='div'
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />

      <Dialog open={openEditDialog} onClose={handleEditDialogClose}>
        <DialogTitle>Edit User</DialogTitle>
        <DialogContent>
          <form>
            <Grid container spacing={5}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Email'
                  placeholder='leonard.carter@example.com'
                  value={selectedRow.email}
                  onChange={(e) =>
                    setSelectedRow((prevRow) => ({ ...prevRow, email: e.target.value }))
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='First Name'
                  placeholder='Leonard'
                  value={selectedRow.first_name}
                  onChange={(e) =>
                    setSelectedRow((prevRow) => ({ ...prevRow, first_name: e.target.value }))
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Last Name'
                  placeholder='Carter'
                  value={selectedRow.last_name}
                  onChange={(e) =>
                    setSelectedRow((prevRow) => ({ ...prevRow, last_name: e.target.value }))
                  }
                />
              </Grid>
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleEditDialogClose}>Cancel</Button>
          <Button onClick={handleEditDialogSave}>Save</Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openCreateDialog} onClose={handleCreateDialogClose}>
        <DialogTitle>Create User</DialogTitle>
        <DialogContent>
          <form>
            <Grid container spacing={5}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Email'
                  placeholder='leonard.carter@example.com'
                  value={newUser.email || ''}
                  onChange={(e) => setNewUser({ ...newUser, email: e.target.value })}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='First Name'
                  placeholder='Leonard'
                  value={newUser.first_name || ''}
                  onChange={(e) => setNewUser({ ...newUser, first_name: e.target.value })}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Last Name'
                  placeholder='Carter'
                  value={newUser.last_name || ''}
                  onChange={(e) => setNewUser({ ...newUser, last_name: e.target.value })}
                />
              </Grid>
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCreateDialogClose}>Cancel</Button>
          <Button onClick={handleCreateDialogSave}>Save</Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openDeleteDialog} onClose={handleDeleteDialogClose}>
        <DialogTitle>Delete User</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete this user?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteDialogClose}>No</Button>
          <Button onClick={handleDeleteDialogConfirm}>Yes</Button>
        </DialogActions>
      </Dialog>
    </Paper>
  );
};

export default TableCustomer;
