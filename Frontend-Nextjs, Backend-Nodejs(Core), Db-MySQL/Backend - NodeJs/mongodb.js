const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/mydatabase', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB database');
});

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  f_name: {
    type: String
  },
  l_name: {
    type: String
  }
});

userSchema.index({ email: 1 }, { unique: true });
userSchema.index({ f_name: '2dsphere' });
userSchema.index({ l_name: '2dsphere' });

const User = mongoose.model('User', userSchema);